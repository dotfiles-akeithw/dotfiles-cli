set enc=utf-8
set fenc=utf-8
set termencoding=utf-8

set nocompatible
set autoindent
set smartindent
set tabstop=2
set shiftwidth=2
set textwidth=120

set t_Co=256
syntax on
set number
set showmatch
set bg=light
